﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using SetupSQLite;
using SQLite;
using Xamarin.Forms;

namespace ASSN3
{
	public class WebURL
	{
        [SQLite.PrimaryKey, SQLite.AutoIncrement]
		public int Id { get; set; }

		public string Url { get; set; }
		public string Images { get; set; }
		public string Title { get; set; }
    }

	public partial class ASSN3Page : ContentPage 
	{
        private SQLiteAsyncConnection _connection;
        public List<WebURL> urls;

		public ASSN3Page()
		{
            InitializeComponent();

            _connection = DependencyService.Get<ISQLiteDb>().GetConnection();
        }

        async void ReloadPicker()
        {
            await _connection.CreateTableAsync<WebURL>();

            urls = await _connection.Table<WebURL>().ToListAsync();

            if (ShowUrls.Items.Count != 0)
            {
                ShowUrls.Items.Clear();
            }

            foreach (var x in urls)
            {
                ShowUrls.Items.Add($"{x.Title}");
            }
        }        

		async void OpenWebView(object sender, System.EventArgs e)
		{
            if (ShowUrls.SelectedIndex < 0)
            {
                DisplayAlert("ALERT", "You need to Select a URL before you can do this", "OK");
            }
            else
            {
                var passOnUrl = urls[ShowUrls.SelectedIndex].Url;
                await Navigation.PushModalAsync(new WebViewShow(passOnUrl));
            }
		}

		async void AddURLAction(object sender, System.EventArgs e)
		{
			await Navigation.PushModalAsync(new AddURLPage());
		}

		async void UpdateURLAction(object sender, System.EventArgs e)
		{
            if (ShowUrls.SelectedIndex < 0)
            {
                DisplayAlert("ALERT", "You need to Select a URL before you can do this", "OK");
            }
            else
            {
                urls = await _connection.Table<WebURL>().ToListAsync();

                var selectedClass = urls[ShowUrls.SelectedIndex];
                var selectedItems = ShowUrls.SelectedIndex;
                selectedClass = urls[ShowUrls.SelectedIndex];

                await Navigation.PushModalAsync(new EditURLPage(selectedClass, selectedItems));
            }
		}

		async void RemoveUrls(object sender, System.EventArgs e)
		{
            if (ShowUrls.SelectedIndex < 0)
            {
                DisplayAlert("ALERT", "You need to Select a URL before you can do this", "OK");
            }
            else
            {
                urls = await _connection.Table<WebURL>().ToListAsync();

                var selectedItems = urls[ShowUrls.SelectedIndex];

                await _connection.DeleteAsync(selectedItems);

                ReloadPicker();

                DisplayAlert("Deleted", "The selected URL Has been Deleted", "OK");
            }
		}
        protected override void OnAppearing()
		{
			ReloadPicker();

			base.OnAppearing();
		}

        
    }
}