﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace ASSN3
{
	public partial class WebViewShow : ContentPage
	{
		public WebViewShow(string passOnUrl)
		{
			InitializeComponent();

            ShowWeb.Source = passOnUrl;
            System.Diagnostics.Debug.WriteLine($"**NOTICE**{passOnUrl}**NOTICE**");
        }

		async void GoBack(object sender, EventArgs e)
		{
			await Navigation.PopModalAsync();
		}
	}
}
